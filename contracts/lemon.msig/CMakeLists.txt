add_contract(lemon.msig lemon.msig ${CMAKE_CURRENT_SOURCE_DIR}/src/lemon.msig.cpp)

target_include_directories(lemon.msig
   PUBLIC
   ${CMAKE_CURRENT_SOURCE_DIR}/include)

set_target_properties(lemon.msig
   PROPERTIES
   RUNTIME_OUTPUT_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}")
