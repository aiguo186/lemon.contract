add_contract(lemon.token lemon.token ${CMAKE_CURRENT_SOURCE_DIR}/src/lemon.token.cpp)

target_include_directories(lemon.token
   PUBLIC
   ${CMAKE_CURRENT_SOURCE_DIR}/include)

set_target_properties(lemon.token
   PROPERTIES
   RUNTIME_OUTPUT_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}")
